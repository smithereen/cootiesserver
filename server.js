var http = require('http');

    
var express = require('express'), 
    control = require('./controllers/queries');

var app = express();

app.configure( function() 
{
    app.use(express.logger("dev"));
	app.use(express.bodyParser());
});

/*********************************************/

app.post('/add', control.add);
app.post('/find', control.find);
app.post('/', control.find);

app.listen(8090);

